#include "bridge.h"
#include <QCoreApplication>
#include <QFile>
#include <QFont>
#include <QFontDatabase>
#include <QApplication>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>

Bridge bridge;

int main(int argc, char *argv[]) {
	if (!QFile::exists("retro.img")) {
		bridge.prepareImage();
	}

	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

	QApplication app(argc, argv);
	app.setOrganizationName("KDE");
	app.setOrganizationDomain("org.kde");

	qRegisterMetaType<Bridge *>();
	qmlRegisterUncreatableType<Bridge>("org.kde.retroplayground", 1, 0,
					   "Bridge",
					   "use the global variable u wacko");

	const QFont fixedFont =
	    QFontDatabase::systemFont(QFontDatabase::FixedFont);

	QQmlApplicationEngine engine;
	const QUrl url(QStringLiteral("qrc:/Main.qml"));
	// clang-format off
	QObject::connect(
		&engine, &QQmlApplicationEngine::objectCreated, &app,
		[url](QObject *obj, const QUrl &objUrl) {
			if ((obj == nullptr) && url == objUrl) {
				QCoreApplication::exit(-1);
			}
		},
		Qt::QueuedConnection);
	// clang-format on
	engine.rootContext()->setContextProperty("bridge",
						 QVariant::fromValue(&bridge));
	engine.rootContext()->setContextProperty("monoFont", fixedFont);
	engine.load(url);

	return app.exec();
}
