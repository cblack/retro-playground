import QtQuick 2.15
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC2
import org.kde.kirigami 2.12 as Kirigami
import Qt.labs.settings 1.0

import org.kde.retroplayground 1.0

Kirigami.AbstractApplicationWindow {
	id: mu

	Settings {
		property alias toolbarText: field.text
		property alias bodyText: mainArea.text
	}

	ColumnLayout {
		anchors.fill: parent

		QQC2.ToolBar {
			QQC2.TextField {
				id: field

				text: "empty define"

				persistentSelection: true

				horizontalAlignment: Qt.AlignHCenter
				verticalAlignment: Qt.AlignVCenter

				font: monoFont
				background: null

				anchors.fill: parent

				MouseArea {
					anchors.fill: parent
					acceptedButtons: Qt.MiddleButton

					onClicked: (e) => {
						field.cursorPosition = field.positionAt(e.x, e.y)
						field.selectWord()

						if (mainArea.selection === "") {
							mainArea.insert(mainArea.cursorPosition, bridge.evaluate(field.selectedText))
						} else {
							console.info(`'${mainArea.selectedText} ${field.selectedText}`)
							mainArea.insert(mainArea.cursorPosition, bridge.evaluate(`'${mainArea.selectedText} ${field.selectedText}`))
						}

						e.accepted = true
					}
				}
			}
			Layout.fillWidth: true
		}

		QQC2.TextArea {
			id: mainArea

			objectName: "texty"

			font: monoFont
			persistentSelection: true

			background: null

			Component.onCompleted: bridge.bindTextDocument(textDocument, this)

			function getLine() {
				let splitLine = text.split('\n')
				let totalLength = 0

				for (let i = 0; i < splitLine.length; i++) {
					totalLength += splitLine[i].length

					if (totalLength >= cursorPosition) {
						return splitLine[i]
					}
				}

				return ""
			}

			Keys.onEnterPressed: {
				let cont = ""

				if (selectedText === "") {
					cont = bridge.evaluate(getLine())
				} else {
					cont = bridge.evaluate(selectedText)
				}

				mu.showPassiveNotification("ok")

				insert(cursorPosition, "" + cont)
			}

			Layout.fillWidth: true
			Layout.fillHeight: true
		}
	}
}
