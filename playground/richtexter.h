#pragma once

#include <QRegularExpression>
#include <QTextCharFormat>
#include <QImage>
#include <QTextDocument>

class TextFormatter : public QObject {
	Q_OBJECT

public:
	explicit TextFormatter(QTextDocument* parent, QObject* field);
	~TextFormatter();

private:
	class Private;
	Private* p;

	void handleTextChanged(int position, int charsRemoved, int charsAdded);
};
