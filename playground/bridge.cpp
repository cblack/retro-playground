#include <QProcess>
#include <QString>
#include <QTemporaryFile>
#include <QDebug>

#include "bridge.h"

void Bridge::prepareImage()
{
	QProcess proc;
	proc.start("retro", QStringList{});
	proc.waitForStarted();
	proc.write(R"(

(crc_gave_these_comments_to_me._pretty_chill_dude_on_IRC.)

{{
    :back-to-normal &interpret unhook ;

---reveal---
    :prefix:(
        fetch $* eq?
        [
            [ '*) s:eq? &back-to-normal if ] &interpret set-hook
        ] if
    ; immediate
}}

:marker (s-)
    ': s:prepend
    '_# s:append
    @Dictionary n:to-string s:append
    '_ s:append
    '!Dictionary s:append
    '_; s:append
    s:evaluate
;

:empty (-)
    '---marker---_'---marker---_marker s:evaluate
;

:define (s-)
    'retro-describe_ s:prepend unix:system
;

'---marker--- marker

'retro.img image:save

bye
)");
	proc.waitForFinished();
}

QString Bridge::evaluate(const QString &mu) {
	QTemporaryFile file;
	if (!file.open()) {
		return {};
	}

	auto tmpl = QString("~~~ \n%1 ;\n 'retro.img image:save ~~~").arg(mu);
	file.write(tmpl.toLocal8Bit());
	file.flush();

	auto cmd = QStringList{"-u", "retro.img", "-f", file.fileName()};

	QProcess proc;
	proc.setProcessChannelMode(QProcess::MergedChannels);
	proc.start("retro", cmd);
	if (!proc.waitForFinished()) {
		qDebug() << "failed to finish";
	}

	return QString::fromLocal8Bit(proc.readAllStandardOutput());
}
