#include "richtexter.h"

#include <QDebug>
#include <QEvent>
#include <QSet>
#include <QTextCursor>
#include <QKeyEvent>
#include <QKeySequence>
#include <QScopeGuard>

class TextFormatter::Private {
public:
	QTextDocument* parent = nullptr;
	QObject* field = nullptr;
	bool ignoreSignals = false;
};

TextFormatter::TextFormatter(QTextDocument* parent, QObject* field)
{
	p = new Private;

	p->parent = parent;
	p->field = field;

	field->installEventFilter(this);

	connect(parent, &QTextDocument::contentsChange, this, &TextFormatter::handleTextChanged);

	handleTextChanged(0, 0, 0);
}

TextFormatter::~TextFormatter()
{
	delete p;
}

void TextFormatter::handleTextChanged(int position, int charsRemoved, int charsAdded)
{
	if (p->ignoreSignals) return;

	p->ignoreSignals = true;

	QTextCursor curs(p->parent);
	curs.setPosition(0);
	curs.movePosition(QTextCursor::MoveOperation::End, QTextCursor::MoveMode::KeepAnchor);
	curs.setCharFormat(QTextCharFormat());

	auto plaintextBuffer = curs.selectedText();

	QTextCharFormat mu;
	mu.setForeground(QColor("#3dd425"));
	bool isWhiteSpace = true;

	for (int i = 0; i < plaintextBuffer.length(); i++) {
		curs.setPosition(i);
		curs.movePosition(QTextCursor::MoveOperation::Right, QTextCursor::MoveMode::KeepAnchor);

		auto sg = qScopeGuard([&]{ curs.setCharFormat(mu); });

		if (!plaintextBuffer[i].isSpace() && !isWhiteSpace) {
			continue;
		} else if (isWhiteSpace && !plaintextBuffer[i].isSpace()) {
			isWhiteSpace = false;
			switch (plaintextBuffer[i].unicode()) {
			case ':': {
				mu.setForeground(QColor("#e93d58"));
				break;
			}
			case '(': {
				auto color = p->field->property("color").value<QColor>();
				color.setAlphaF(0.6);
				mu.setForeground(color);
				break;
			}
			case '\'': {
				mu.setForeground(QColor("#e9643a"));
				break;
			}
			case '#': {
				auto color = p->field->property("color").value<QColor>();
				mu.setForeground(color);
				mu.setFontWeight(900);
				break;
			}
			default: {
				mu.setForeground(QColor("#3dd425"));
				break;
			}
			}
		} else if (plaintextBuffer[i].isSpace()) {
			isWhiteSpace = true;
			continue;
		}
	}

	p->ignoreSignals = false;

}
