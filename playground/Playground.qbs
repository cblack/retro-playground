QtApplication {
	name: "retro-playground"

	files: [
		"*.cpp",
		"*.h",
	]

	Group {
		files: ["*.qml"]
		fileTags: "qt.core.resource_data"
		Qt.core.resourcePrefix: "/"
	}

	Depends { name: "Qt"; submodules: ["gui", "widgets", "quick", "quickcontrols2", "qml"] }
}
