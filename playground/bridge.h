#pragma once

#include <QObject>
#include <QQuickTextDocument>

#include "richtexter.h"

class Bridge : public QObject
{
	Q_OBJECT

public:
	Q_INVOKABLE void prepareImage();
	Q_INVOKABLE QString evaluate(const QString& mu);
	Q_INVOKABLE void bindTextDocument(QQuickTextDocument* doc, QObject* field)
	{
		new TextFormatter(doc->textDocument(), field);
	}
};
